// Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.

const ar = [2, 5, -2, 0, 8];

function double(x) {
    return x *= 2; 
}

function map(fn, array) {
    const newAr = [];
    for (let i = 0; i < array.length; i++) {
        newAr[i] = fn(array[i]);
    }
    return newAr;
}

document.write(`Маємо масив: ${ar}`);
document.write(`<br> Обробимо його функцією: y = 2x `)
document.write(`<br> Новий массив: ${map(double, ar)}`);

document.write("<hr>");


// Використовуючи CallBack function, створіть калькулятор, який буде від користувача приймати 2 числа і знак арифметичної операції. При введенні не числа або при розподілі на 0 виводити помилку.



let isInputError = function (arg) {
    if (isNaN(arg)) {
        alert("Це не число. А треба число!");
        return true;
    }   
    return false; 
}

let isWrongOperation = (operation, operand2) => {
    if (!["+", "-", "*", "/"].includes(operation)) {
        alert("Незрозуміла операция. Спробуте ще раз");
        return true;
    }
    if (operation == "/" && operand2 == 0) {
        alert("Помилка. Ділення на нуль");
            return true;
        }
}

function add(a, b) {return a + b;}
function subtract(a, b) {return a - b;}
function multiply(a, b) {return a * b;}
function divide(a, b) {return a / b;}


function calculate(a, b, operation) {
    switch (operation) {
        case "+": alert(a + " + " + b  + " = " + add(a, b)); break;
        case "-": alert(a + " - " + b  + " = " + subtract(a, b)); break;
        case "*": alert(a + " * " + b  + " = " + multiply(a, b)); break;
        case "/": alert(a + " / " + b  + " = " + divide(a, b)); break;
    }
}

function input(arg) { 
    do {
        var a = arg == 1 ? prompt("Робимо калькулятор. Введіть перше число:") : prompt("Введіть друге число:");
    } while (isInputError(a));   
    return parseInt(a);
}

function inputOperation(operand2) {
    do {
        var operation = prompt("Що будемо робити з числами? (+ - * /):");
    } while (isWrongOperation(operation, operand2));
    return operation;
}


// ----------------------------------------------------------------
do{
calculate(input(1), operand2 = input(2), inputOperation(operand2));
} while (confirm ("Будемо ще рахувати?"));

